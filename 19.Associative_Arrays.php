<html>
   <body>
      
      <?php
         /* First method to associate create array. */
         $salaries = array("mani" => 2000, "srinu" => 1000, "ajay" => 500);
         
         echo "Salary of mani is ". $salaries['mani'] . "<br />";
         echo "Salary of srinu is ".  $salaries['srinu']. "<br />";
         echo "Salary of ajay is ".  $salaries['ajay']. "<br />";
         
         /* Second method to create array. */
         $salaries['mani'] = "high";
         $salaries['srinu'] = "medium";
         $salaries['ajay'] = "low";
         
         echo "Salary of mani is ". $salaries['mani'] . "<br />";
         echo "Salary of srinu is ".  $salaries['srinu']. "<br />";
         echo "Salary of ajay is ".  $salaries['ajay']. "<br />";
      ?>
   
   </body>
</html>