<html>
   <body>
      
      <?php
         $marks = array( 
            "mani" => array (
               "physics" => 35,
               "maths" => 30,	
               "chemistry" => 39
            ),
            
            "ravi" => array (
               "physics" => 30,
               "maths" => 32,
               "chemistry" => 29
            ),
            
            "akhil" => array (
               "physics" => 31,
               "maths" => 22,
               "chemistry" => 39
            )
         );
         
         /* Accessing multi-dimensional array values */
         echo "Marks for mani in physics : " ;
         echo $marks['mani']['physics'] . "<br />"; 
         
         echo "Marks for ravi in maths : ";
         echo $marks['ravi']['maths'] . "<br />"; 
         
         echo "Marks for akhil in chemistry : " ;
         echo $marks['akhil']['chemistry'] . "<br />"; 
      ?>
   
   </body>
</html>